package main

import (
	"fmt"

	"github.com/crazyoptimist/go_package_poc"
)

func main() {
	message := go_package_poc.Hello("Mr. Docker Captain")
	fmt.Println(message)
}
